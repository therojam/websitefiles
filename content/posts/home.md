---
title: "Home"
date: 2021-06-13T21:21:01+02:00
draft: false 
---
# Say Hello to Leipzig!
### the living place of Anton Müller

Congratulations! You have reached my personal website. 

### Find out about Me!

On this page you'll find some information about me!
You're looking for my old page? Its on [old.am-news.eu](https://old.am-news.eu)!

More on the my [about page](/about-me).
